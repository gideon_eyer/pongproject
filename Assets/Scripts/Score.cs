﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public float player1Lives = 3;
    public float player2Lives = 3;

    public int player1Score = 0;
    public int player2Score = 0;
    public int pointsToAdd = 100;
    public int pointsMultiplier = 2;
    public int nextScene;

    public Text player1Text;
    public Text player2Text;
    public Text player1ScoreText;
    public Text player2ScoreText;
    
	// Use this for initialization
	void Start ()
    {
        updateScore();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("DoublePoints"))
        {
            Debug.Log("We collided");
            pointsToAdd = pointsToAdd * pointsMultiplier;
        }

        if (other.transform.CompareTag("RightWall"))
        {
            player1Lives -= 1;
        }

        if (other.transform.CompareTag("LeftWall"))
        {
            player2Lives -= 1;
        }

        if(player1Lives <= 0 || player2Lives <= 0 )
        {
            SaveScore();
            SceneManager.LoadScene(nextScene);
        }

        updateScore();
    }

    public void OnCollisionEnter(Collision col)
    {
        if (col.transform.CompareTag("Left"))
        {
            AddPointsPlayer1(pointsToAdd);
        }

        if (col.transform.CompareTag("Right"))
        {
            AddPointsPlayer2(pointsToAdd);
        }

        updateScore();
    }

    public void updateScore()
    {
        player1Text.text = "Player1 Lives: " + player1Lives.ToString();
        player2Text.text = "Player2 Lives: " + player2Lives.ToString();
        player1ScoreText.text = "Score: " + player1Score.ToString();
        player2ScoreText.text = "Score: " + player2Score.ToString();
    }

    public void SaveScore()
    {
        if (PlayerPrefs.HasKey("HighScore"))
        {
            if(player1Score > player2Score)
            {
                PlayerPrefs.SetInt("HighScore", player1Score);
            }
            else
            {
                PlayerPrefs.SetInt("HighScore", player2Score);
            }
        }
        else if(player1Score > player2Score)
        {
            PlayerPrefs.SetInt("HighScore", player1Score);
        }
        else
        {
            PlayerPrefs.SetInt("HighScore", player2Score);
        }

    }

    public void AddPointsPlayer1(int pointsToAdd)
    {
        player1Score += pointsToAdd;
    }

    public void AddPointsPlayer2(int pointsToAdd)
    {
        player2Score += pointsToAdd;
    }
}
